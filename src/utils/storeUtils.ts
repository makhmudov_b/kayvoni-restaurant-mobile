import createNextState from "immer";

export const getInitialState = (name, initState) => initState;

export function createReducer(initialState: any, actionsMap) {
  return function (state = initialState, action) {
    return createNextState(state, (draft) => {
      const caseReducer = actionsMap[action.type];

      if (caseReducer) {
        return caseReducer(draft, action);
      }

      return draft;
    });
  };
}
