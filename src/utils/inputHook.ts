import { useState } from "react";

export function useCustomInput() {
  const [value, setValue] = useState("");
  const onChange = (text) => {
    setValue(text);
  };
  return {
    value,
    onChange,
  };
}
