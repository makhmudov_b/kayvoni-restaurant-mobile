import global from "../resources/global";

export const getStatusColor = (status: any) => {
  if (status == 0) return global.colors.main;
  if (status == 1) return global.colors.lightYellow;
  if (status == 3) return global.colors.darkBlue;
  if (status == 2) return global.colors.lightGreen;
  if (status == -2) return global.colors.red;
  if (status == -3) return global.colors.red;
};
export const getStatusName = (status) => {
  if (status == 0) return "Создан";
  if (status == 1) return "В ожидании";
  if (status == 2) return "Принят";
  if (status == 3) return "Закончен";
  if (status == -2) return "Отклонён";
  if (status == -3) return "Возврат денег";
};
