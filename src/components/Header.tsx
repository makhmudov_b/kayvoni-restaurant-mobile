import React from "react";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";
import CustomStatusBar from "./CustomStatusBar";
import { View } from "react-native";

import { HeaderWrap, TextFiled } from "../resources/styled";
import global from "../resources/global";

import Logo from "../../assets/images/logo.svg";
import ArrowBack from "../../assets/images/arrow-back.svg";

export default ({ title = "`KAYVONI`", enableBack = false }) => {
  const navigation = useNavigation();
  return (
    <>
      <CustomStatusBar />
      <HeaderWrap>
        <View style={{ flex: 1, justifyContent: "center" }}>
          {enableBack && (
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <ArrowBack />
            </TouchableOpacity>
          )}
        </View>
        <View>
          <TextFiled size="16px" color="white" weight="700">
            KAYVONI
          </TextFiled>
        </View>
        <View
          style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}
        >
          <Logo onPress={() => navigation.navigate("Home")} />
        </View>
      </HeaderWrap>
    </>
  );
};
