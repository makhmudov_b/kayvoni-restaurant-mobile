import React from "react";
import { View, StatusBar, Platform } from "react-native";
import Constants from "expo-constants";
import global from "../resources/global";

export default function CustomStatusBar() {
  return (
    <>
      <StatusBar barStyle={"light-content"} />
      {Platform.OS == "ios" && (
        <View
          style={{
            height: Constants.statusBarHeight,
            backgroundColor: global.colors.main,
          }}
        />
      )}
    </>
  );
}
