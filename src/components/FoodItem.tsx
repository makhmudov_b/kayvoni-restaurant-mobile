import React, { useEffect, useState } from "react";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Image } from "react-native";
import global from "../resources/global";
import { FoodMenu, TextFiled, FoodImage } from "../resources/styled";
import Show from "../../assets/images/show.svg";
import Empty from "../../assets/images/empty.svg";
export default function FoodItem(props: any) {
  const { food, enableHandle, index } = props;

  const [active, setActive] = useState(true);

  useEffect(() => {
    parseInt(food.enabled) === 0 ? setActive(false) : setActive(true);
  }, []);

  const swichFood = () => {
    enableHandle(food.id, setActive(!active));
  };

  return (
    <FoodMenu key={food.id}>
      <FoodImage>
        <Image
          source={{ uri: food.image }}
          style={{
            width: 44,
            height: 44,
            borderRadius: 44 / 2,
          }}
        />
      </FoodImage>
      <TextFiled size={"12px"} style={{ maxWidth: 150 }}>
        {food.name_ru}
      </TextFiled>
      <TouchableOpacity
        onPress={() => swichFood()}
        style={{
          width: 39,
          height: 39,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: `${
            !!active ? global.colors.lightGreen : global.colors.textColor2
          }`,
          borderRadius: 39 / 2,
        }}
      >
        {!!active ? <Show /> : <Empty />}
      </TouchableOpacity>
    </FoodMenu>
  );
}
