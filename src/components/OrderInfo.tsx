import React from "react";
import global from "../resources/global";

import { RowBlock, TextFiled } from "../resources/styled";

type ParamList = {
  user?: any;
  time?: any;
};

export default function OrderInfo(props: ParamList) {
  const { user, time } = props;

  return (
    <>
      <RowBlock>
        <TextFiled size="14px" weight="700">
          Бронь на дату:{" "}
        </TextFiled>
        <TextFiled size="14px">{time.slice(0, -3)} </TextFiled>
      </RowBlock>
      <RowBlock>
        <TextFiled size="14px" weight="700">
          Имя посетителя:
        </TextFiled>
        <TextFiled style={{ marginRight: 3 }} size="14px">
          {user.name}
        </TextFiled>
      </RowBlock>
      <TextFiled
        size="14px"
        color={global.colors.textColor2}
        style={{ marginBottom: 20 }}
      >
        +998{user.phone}
      </TextFiled>
    </>
  );
}
