import * as foodActionTypes from "../actionTypes/foodActionTypes";
import { createReducer } from "../../utils/storeUtils";

const initState = {
  foods: [],
  loading: false,
  switchLoading: false,
  error: null,
  enabled: true,
};

const reducers = {
  [foodActionTypes.GET_FOOD_REQUEST](state) {
    state.loading = true;
    state.error = null;
  },
  [foodActionTypes.GET_FOOD_SUCCESS](state, action) {
    state.foods = action.payload.data;
    state.loading = false;
  },
  [foodActionTypes.GET_FOOD_ERROR](state, action) {
    state.loading = false;
    state.error = action.error;
    state.foods = [];
  },
  [foodActionTypes.SWITCH_FOOD_REQUEST](state) {
    state.switchLoading = true;
    state.error = null;
  },
  [foodActionTypes.SWITCH_FOOD_SUCCESS](state, action) {
    state.switchLoading = false;
  },
  [foodActionTypes.SWITCH_FOOD_ERROR](state, action) {
    state.switchLoading = false;
    state.error = action.error;
  },
};

export default createReducer(initState, reducers);
