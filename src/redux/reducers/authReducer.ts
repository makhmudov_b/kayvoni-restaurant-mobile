import * as authActionTypes from "../actionTypes/authActionTypes";
import { createReducer } from "../../utils/storeUtils";

const initState = {
  token: null,
  loading: false,
  error: null,
};

const reducers = {
  [authActionTypes.AUTH_REQUEST](state) {
    state.loading = true;
  },
  [authActionTypes.AUTH_SUCCESS](state, action) {
    state.token = action.payload.accessToken;
    state.loading = false;
  },
  [authActionTypes.AUTH_LOGOUT](state) {
    state.token = null;
    state.loading = false;
  },
  [authActionTypes.AUTH_ERROR](state, action) {
    state.loading = false;
    state.error = action.error;
    state.token = null;
  },
};

export default createReducer(initState, reducers);
