import * as profileActionTypes from "../actionTypes/profileActionTypes";
import { getInitialState, createReducer } from "../../utils/storeUtils";

const initState = {
  restaurant: null,
  profit: 0,
  ordersCount: 0,
  loading: false,
  error: null,
};

const reducers = {
  [profileActionTypes.PROFILE_REQUEST](state) {
    state.loading = true;
  },
  [profileActionTypes.PROFILE_SUCCESS](state, action) {
    state.loading = false;
    state.restaurant = action.payload.data.restaurant;
    state.ordersCount = action.payload.data.order_count;
    state.profit = action.payload.data.profit;
  },
  [profileActionTypes.PROFILE_ERROR](state, action) {
    state.error = action.error;
    state.profit = 0;
    state.ordersCount = 0;
    state.loading = false;
  },
};

export default createReducer(initState, reducers);
