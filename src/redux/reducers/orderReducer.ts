import * as orderActionTypes from "../actionTypes/orderActionTypes";
import { createReducer } from "../../utils/storeUtils";

const initState = {
  loading: false, // calendar loading
  ordersLoading: false, // orders loading
  innerLoading: false, // show order loading
  error: null, // calendar error
  ordersError: null,
  innerError: null,
  orders: [],
  order: null,
  calendar: {},
};

const reducers = {
  [orderActionTypes.GET_ORDER_REQUEST](state) {
    state.ordersLoading = true;
  },
  [orderActionTypes.GET_ORDER_CALENDAR_REQUEST](state) {
    state.loading = true;
  },
  [orderActionTypes.DENY_ORDER_REQUEST](state) {
    state.innerLoading = true;
  },
  [orderActionTypes.ACCEPT_ORDER_REQUEST](state) {
    state.innerLoading = true;
  },
  [orderActionTypes.GET_ORDER_BY_ID_REQUEST](state) {
    state.innerLoading = true;
  },
  [orderActionTypes.GET_ORDER_SUCCESS](state, action) {
    state.orders = action.payload.data;
    state.ordersLoading = false;
  },
  [orderActionTypes.GET_ORDER_CALENDAR_SUCCESS](state, action) {
    state.calendar = action.payload.data;
    state.loading = false;
  },
  [orderActionTypes.GET_ORDER_BY_ID_SUCCESS](state, action) {
    state.innerLoading = false;
    state.order = action.payload.data;
  },
  [orderActionTypes.GET_ORDER_CALENDAR_ERROR](state, action) {
    state.loading = false;
    state.error = action.error;
  },
  [orderActionTypes.GET_ORDER_ERROR](state, action) {
    state.ordersError = action.error;
    state.ordersLoading = false;
  },
  [orderActionTypes.DENY_ORDER_ERROR](state, action) {
    state.innerLoading = false;
    state.innerError = action.error;
  },
  [orderActionTypes.ACCEPT_ORDER_ERROR](state, action) {
    state.innerLoading = false;
    state.innerError = action.error;
  },
  [orderActionTypes.GET_ORDER_BY_ID_ERROR](state, action) {
    state.innerLoading = false;
    state.innerError = action.error;
  },
};

export default createReducer(initState, reducers);
