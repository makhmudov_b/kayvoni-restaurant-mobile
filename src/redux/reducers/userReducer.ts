import * as userActionTypes from "../actionTypes/userActionTypes";
import { createReducer } from "../../utils/storeUtils";

const initState = {
  orders_count: 0,
  user: {},
  loading: false,
  error: null,
  notification: null,
};

const reducers = {
  [userActionTypes.GET_USER_INFO_REQUEST](state) {
    state.loading = true;
  },
  [userActionTypes.SET_USER_NOTIFICATION](state, action) {
    state.notification = action.payload;
  },

  [userActionTypes.GET_USER_INFO_SUCCESS](state, action) {
    state.user = action.payload.data.user;
    state.orders_count = action.payload.data.orders_count;
    state.loading = false;
  },
  [userActionTypes.GET_USER_INFO_ERROR](state, action) {
    state.error = action.error;
    state.user = {};
    state.orders_count = 0;
    state.loading = false;
  },
};

export default createReducer(initState, reducers);
