import { combineReducers } from "redux";
import auth from "./authReducer";
import user from "./userReducer";
import food from "./foodReducer";
import order from "./orderReducer";
import profile from "./profileReducer";
import reason from "./reasonReducer";

export default combineReducers({ auth, user, food, order, profile, reason });
