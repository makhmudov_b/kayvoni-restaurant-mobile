import * as profileActionTypes from "../actionTypes/profileActionTypes";
import { getInitialState, createReducer } from "../../utils/storeUtils";

const initState = {
  reason: null,
  loading: false,
  error: null,
};

const reducers = {
  [profileActionTypes.REASON_REQUEST](state) {
    state.loading = true;
    state.error = null;
  },
  [profileActionTypes.REASON_SUCCESS](state, action) {
    state.reason = action.payload.data;
    state.loading = false;
  },
  [profileActionTypes.REASON_ERROR](state, action) {
    state.error = action.error;
    state.reason = null;
    state.loading = false;
  },
};

export default createReducer(initState, reducers);
