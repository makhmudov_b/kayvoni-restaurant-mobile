export const GET_FOOD_REQUEST = "GET_FOOD_REQUEST";
export const GET_FOOD_SUCCESS = "GET_FOOD_SUCCESS";
export const GET_FOOD_ERROR = "GET_FOOD_ERROR";
export const SWITCH_FOOD_REQUEST = "SWITCH_FOOD_REQUEST";
export const SWITCH_FOOD_SUCCESS = "SWITCH_FOOD_SUCCESS";
export const SWITCH_FOOD_ERROR = "SWITCH_FOOD_ERROR";
