import * as foodActionTypes from "../actionTypes/foodActionTypes";
import { getFoods, switchFoods } from "../../api/foodApi";

export const getFoodList = () => (dispatch: any) => {
  return dispatch({
    api: getFoods,
    types: [
      foodActionTypes.GET_FOOD_REQUEST,
      foodActionTypes.GET_FOOD_SUCCESS,
      foodActionTypes.GET_FOOD_ERROR,
    ],
    query: {},
  });
};

export const swichFoodById = (id, callback?) => (dispatch: any) => {
  return dispatch({
    api: switchFoods,
    types: [
      foodActionTypes.SWITCH_FOOD_REQUEST,
      foodActionTypes.SWITCH_FOOD_SUCCESS,
      foodActionTypes.SWITCH_FOOD_ERROR,
    ],
    query: id,
  }).then((response) => {
    if (response.responseStatus === 200) {
      if (callback) callback();
    }
  });
};

