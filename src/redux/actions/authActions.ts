// import { push } from "react-router-redux";
import * as authActionTypes from "../actionTypes/authActionTypes";
import { loginUser } from "../../api/authApi";

export const auth = (payload, callback?) => (dispatch) => {
  const { password, phone } = payload;

  dispatch({
    api: loginUser,
    types: [
      authActionTypes.AUTH_REQUEST,
      authActionTypes.AUTH_SUCCESS,
      authActionTypes.AUTH_ERROR,
    ],
    query: {
      phone,
      password,
    },
  }).then((response) => {
    if (response.responseStatus === 200) {
      if (callback) callback();
    }
    // console.log(response);
  });
};

export const logout = (callBack?) => (dispatch) => {
  dispatch({
    type: authActionTypes.AUTH_LOGOUT,
  });
  if (callBack) callBack();
};
export const sucessLogin = (payload) => {
  return {
    type: authActionTypes.AUTH_SUCCESS,
    payload,
  };
};
