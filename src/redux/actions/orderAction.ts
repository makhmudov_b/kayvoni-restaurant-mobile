import * as orderActionTypes from "../actionTypes/orderActionTypes";
import { accept, deny, getOrders, orderById } from "../../api/orderApi";

export const getOrderList = () => (dispatch: any) => {
  return dispatch({
    api: getOrders,
    types: [
      orderActionTypes.GET_ORDER_REQUEST,
      orderActionTypes.GET_ORDER_SUCCESS,
      orderActionTypes.GET_ORDER_ERROR,
    ],
    query: {},
  });
};

export const getOrderListWithDate = (from, to) => (dispatch: any) => {
  return dispatch({
    api: getOrders,
    types: [
      orderActionTypes.GET_ORDER_CALENDAR_REQUEST,
      orderActionTypes.GET_ORDER_CALENDAR_SUCCESS,
      orderActionTypes.GET_ORDER_CALENDAR_ERROR,
    ],
    query: { calendar: true },
  });
};

export const getOrderById = (id) => (dispatch: any) => {
  return dispatch({
    api: orderById,
    types: [
      orderActionTypes.GET_ORDER_BY_ID_REQUEST,
      orderActionTypes.GET_ORDER_BY_ID_SUCCESS,
      orderActionTypes.GET_ORDER_BY_ID_ERROR,
    ],
    query: { id },
  });
};

export const acceptOrder = (id, tableNumber, callback) => (dispatch: any) => {
  dispatch({
    api: accept,
    types: [
      orderActionTypes.ACCEPT_ORDER_REQUEST,
      orderActionTypes.ACCEPT_ORDER_SUCCESS,
      orderActionTypes.ACCEPT_ORDER_ERROR,
    ],
    query: { id, tableNumber },
  }).then((response) => {
    if (response.responseStatus === 200) {
      dispatch(getOrderList());
      if (callback) callback();
    }
  });
};

export const denyOrder = (id, reason, callback) => (dispatch: any) => {
  dispatch({
    api: deny,
    types: [
      orderActionTypes.DENY_ORDER_REQUEST,
      orderActionTypes.DENY_ORDER_SUCCESS,
      orderActionTypes.DENY_ORDER_ERROR,
    ],
    query: { id, reason },
  }).then((response) => {
    if (response.responseStatus === 200) {
      dispatch(getOrderList());
      if (callback) callback();
    }
  });
};
