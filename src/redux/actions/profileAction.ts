import * as profileActionTypes from "../actionTypes/profileActionTypes";
import { getStats, getReason } from "../../api/userApi";

export const getStatics = () => (dispatch: any) => {
  return dispatch({
    api: getStats,
    types: [
      profileActionTypes.PROFILE_REQUEST,
      profileActionTypes.PROFILE_SUCCESS,
      profileActionTypes.PROFILE_ERROR,
    ],
    query: {},
  });
};
export const getReasons = () => (dispatch: any) => {
  return dispatch({
    api: getReason,
    types: [
      profileActionTypes.REASON_REQUEST,
      profileActionTypes.REASON_SUCCESS,
      profileActionTypes.REASON_ERROR,
    ],
    query: {},
  });
};
