import * as userActionTypes from "../actionTypes/userActionTypes";
import { getUser, setUser } from "../../api/userApi";

export const getUserInfo = () => (dispatch: any) => {
  return dispatch({
    api: getUser,
    types: [
      userActionTypes.GET_USER_INFO_REQUEST,
      userActionTypes.GET_USER_INFO_SUCCESS,
      userActionTypes.GET_USER_INFO_ERROR,
    ],
    query: {},
  });
};

export const updateUserInfo = (body) => (dispatch: any) => {
  return dispatch({
    api: setUser,
    types: ["SET_REQUEST", "SET_SUCCES", "SET_ERROR"],
    query: body,
  });
};
export const updateNotification = (notification) => (dispatch) => {
  return dispatch({
    type: userActionTypes.SET_USER_NOTIFICATION,
    payload: notification,
  });
};
