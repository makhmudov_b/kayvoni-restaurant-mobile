import * as authActions from "../actions/authActions";
import { store } from "../storeConfig";

const apiMiddleware = ({ dispatch, getState }: any) => (next: any) => (
  action: any
) => {
  if (!action.api || !action.types) {
    return next(action);
  }
  const {
    api,
    types: [START, SUCCESS, ERROR],
    query,
  } = action;

  dispatch({
    type: START,
    query,
  });
  const { token } = getState().auth;
  return api(query, token)
    .then((response: any) => {
      if (response && response.data && response.status === 200) {
        dispatch({
          type: SUCCESS,
          payload: response.data,
        });
        return {
          payload: response.data,
          query,
          responseStatus: response.status,
        };
      } else if (response.data.statusCode === 401) {
        dispatch(authActions.logout());
        throw { response };
      } else {
        throw (
          (response && response.data && { response }) || {
            response: { data: { message: "Что-то не так!" } },
          }
        );
      }
    })
    .catch((error: any) => {
      error.requestData = query;
      dispatch({
        type: ERROR,
        error,
        query,
      });
      if (error.response && error.response.status === 401) {
        dispatch(authActions.logout());
      }
      throw error;
    });
};

export default apiMiddleware;
