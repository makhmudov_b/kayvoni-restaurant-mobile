import { createStore, applyMiddleware } from "redux";
import AsyncStorage from "@react-native-community/async-storage";
import rootReducer from "./reducers/rootReducer";
import { persistStore, persistReducer } from "redux-persist";
import thunk from "redux-thunk";
import apiMiddleware from "./middlewares/apiMiddleware";

const persistConfig = {
  key: "kayvoni",
  storage: AsyncStorage,
  whitelist: ["auth","user"], // which reducer want to store
};

const persist = persistReducer(persistConfig, rootReducer);
export const store = createStore(
  persist,
  applyMiddleware(thunk, apiMiddleware)
);
export const persistor = persistStore(store);
