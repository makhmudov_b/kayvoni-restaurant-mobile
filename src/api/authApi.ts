import HttpClient from "../utils/httpClient";

export const loginUser = (query) => {
  return HttpClient.doPost("/auth", query, null);
};
