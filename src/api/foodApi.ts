import HttpClient from "../utils/httpClient";
import { HttpConfig } from "./httpConfig";

export const getFoods = (query, token) => {
  return HttpClient.doGet("/food", query, token);
};

export const switchFoods = (id, token) => {
  return HttpClient.doPut(`/food/${id}/switch`, null, token);
};
