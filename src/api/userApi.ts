import HttpClient from "../utils/httpClient";
import { HttpConfig } from "./httpConfig";

export const getUser = (query, token) => {
  return HttpClient.doGet("/user", query, token);
};
export const setUser = (query, token) => {
  return HttpClient.doPut("/token", query, token);
};
export const getReason = (query, token) => {
  return HttpClient.doGet("/reason", null, token);
};
export const getStats = (query, token) => {
  return HttpClient.doGet("/statistics", query, token);
};
