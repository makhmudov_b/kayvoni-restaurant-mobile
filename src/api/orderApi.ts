import HttpClient from "../utils/httpClient";

export const getOrders = (query, token) => {
  return HttpClient.doGet("/orders", query, token);
};

export const orderById = ({ id }, token) => {
  return HttpClient.doGet(`/show/order/` + id, null, token);
};

export const accept = ({ id, tableNumber }, token) => {
  return HttpClient.doPut(
    `/order/${id}/accept`,
    { table_number: tableNumber },
    token
  );
};

export const deny = ({ id, reason }, token) => {
  return HttpClient.doPut(`/order/${id}/deny`, { deny_reason: reason }, token);
};
