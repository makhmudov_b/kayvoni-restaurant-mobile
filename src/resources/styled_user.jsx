import React from "react";
import { View, Text } from "react-native";
import styled from "styled-components";
import global from "./global";

// ===================================================== COMPONENTS ===============================================================

// Header-Block

export const HeaderWrapp = styled.View`
  background-color: ${global.colors.main};
  padding: 15px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  height: 60px;
`;

// // Button-Block

// export const Button = styled.TouchableOpacity`
//   flex-direction: row;
//   justify-content: center;
//   align-items: center;
//   min-width: 50px;
//   height: 30px;
//   background-color: ${({ color = global.colors.white }) => color};
//   box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.12);
//   border-radius: 10px;
//   margin-right: 10px;
// `;

// // Category-Block

// export const Category = styled.TouchableOpacity`
//   width: 48%;
//   height: 100px;
//   margin-bottom: 10px;
//   background-color: ${({ color = global.colors.green }) => color};
//   border-top-left-radius: 10px;
//   border-top-right-radius: 10px;
//   border-bottom-left-radius: 10px;
//   flex-direction: row;
//   justify-content: space-between;
//   box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
// `;

// export const CategoryLeft = styled.View`
//   justify-content: center;
//   align-items: center;
//   padding: 5px;
//   max-width: 100px;
// `;

// export const CategoryRight = styled.View`
//   justify-content: flex-end;
//   align-items: flex-end;
//   position: relative;
// `;

// // Food-Block

// export const Food = styled.TouchableOpacity`
//   padding: 10px;
//   margin-top: 10px;
//   flex-direction: row;
//   justify-content: space-between;
//   width: 100%;
//   background-color: ${global.colors.white};
//   box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
//   border-top-left-radius: 20px;
//   border-bottom-left-radius: 20px;
//   border-top-right-radius: 95px;
//   border-bottom-right-radius: 95px;
// `;

// export const FoodImg = styled.View`
//   justify-content: center;
// `;

// // Full-Category

// export const CategoryFull = styled.View`
//   width: 100%;
//   height: 100px;
//   margin-bottom: 10px;
//   background-color: ${({ color = global.colors.green }) => color};
//   border-top-left-radius: 10px;
//   border-top-right-radius: 10px;
//   border-bottom-left-radius: 10px;
//   flex-direction: row;
//   justify-content: space-between;
//   box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
// `;

// export const CategoryFullLeft = styled.View`
//   justify-content: center;
//   align-items: center;
//   padding: 10px;
// `;

// export const CategoryFullRight = styled.View`
//   justify-content: flex-end;
//   align-items: flex-end;
//   position: relative;
// `;

// // Info-Button-Block

// export const InfoButtonBlock = styled.TouchableOpacity`
//   flex-direction: row;
//   justify-content: space-between;
//   align-items: center;
//   height: 35px;
//   background-color: ${global.colors.white};
//   border-radius: 20px;
//   box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.12);
//   margin-right: 10px;
// `;

// // Promotion-Block

// export const Promotion = styled.TouchableOpacity`
//   position: relative;
//   flex: 1;
//   flex-direction: row;
//   justify-content: space-between;
//   box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
//   margin-top: 10px;
// `;

// export const PromotionLeft = styled.View`
//   padding: 10px;
//   width: 85%;
//   background-color: ${global.colors.white};
//   border-top-left-radius: 20px;
//   border-bottom-left-radius: 20px;
// `;

// export const PromotionCenter = styled.View`
//   position: absolute;
//   top: 0;
//   right: 35%;
// `;

// export const PromotionRight = styled.View`
//   position: absolute;
//   right: 0;
// `;

// // Restaurant-Block

// export const Restaurant = styled.TouchableOpacity`
//   width: 48%;
//   align-items: center;
//   position: relative;
//   margin-bottom: 70px;
// `;

// export const RestHead = styled.View`
//   width: 100px;
//   height: 100px;
//   border-radius: 50px;
//   position: absolute;
//   top: -60px;
//   justify-content: center;
//   align-items: center;
//   background-color: ${global.colors.white};
//   z-index: 10;
//   box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
// `;

// export const RestBody = styled.View`
//   height: 175px;
//   background-color: ${global.colors.white};
//   width: 100%;
//   border-radius: 20px;
//   box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
//   padding-top: 50px;
//   padding-left: 10px;
//   justify-content: space-between;
// `;

// // Sort-Button

// export const SortButton = styled.TouchableOpacity`
//   flex-direction: row;
//   justify-content: space-between;
//   align-items: center;
//   width: 100%;
//   height: 30px;
//   background-color: ${({ color = global.colors.white }) => color};
//   box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.12);
//   border-radius: 10px;
//   margin-right: 10px;
//   margin-top: 10px;
// `;

// // Food-Menu

// export const FoodMenuBlock = styled.TouchableOpacity`
//   padding: 10px;
//   margin-top: 10px;
//   flex-direction: row;
//   justify-content: space-between;
//   align-items: center;
//   background-color: ${global.colors.white};
//   width: 100%;
//   height: 40px;
//   border-radius: 20px;
//   box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.12);
// `;

// // FilterBtn

// export const FilterBtn = styled.TouchableOpacity`
//   margin-top: 5px;
//   flex-direction: row;
//   justify-content: space-between;
//   align-items: center;
//   width: 100%;
//   height: 40px;
//   background-color: ${global.colors.white};
//   border-radius: 10px;
//   box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.12);
// `;

// // Check-Btn

// export const CheckBtn = styled.TouchableOpacity`
//   margin-top: 10px;
//   flex-direction: row;
//   justify-content: space-between;
//   align-items: center;
//   width: 100%;
//   height: 40px;
//   background-color: ${({ color = global.colors.white }) => color};
//   border-radius: 10px;
//   box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.12);
// `;

// // ===================================================== SCREENS ===============================================================

// // Home-Screen

// export const Wrapper = styled.View`
//   border-top-right-radius: 20px;
//   border-top-left-radius: 20px;
//   background-color: ${global.colors.white};
//   min-height: 50px;
//   width: 100%;
//   position: relative;
//   top: -15px;
//   padding: 10px;
//   flex-direction: row;
//   justify-content: space-between;
//   flex-wrap: wrap;
// `;

// export const PromotionsWrap = styled.View`
//   padding: 10px;
// `;

// export const RestaurantWrap = styled.View`
//   flex-direction: row;
//   padding: 10px;
//   justify-content: space-between;
//   align-items: center;
//   flex: 1;
//   flex-wrap: wrap;
// `;

// // Category-Screen

// export const CategoryWrapp = styled.View`
//   background-color: ${global.colors.white};
//   min-height: 50px;
//   width: 100%;
//   padding: 10px;
//   flex-direction: row;
//   justify-content: space-between;
//   flex-wrap: wrap;
// `;

// // Rest-Category-Screen

// export const RestCategoryWrap = styled.View`
//   flex-direction: row;
//   padding: 10px;
//   justify-content: space-between;
//   align-items: center;
//   flex: 1;
//   flex-wrap: wrap;
//   margin-top: 15px;
// `;

// // Restaurant-Screen

// export const RestScreenWrap = styled.View`
//   border-top-right-radius: 20px;
//   border-top-left-radius: 20px;
//   background-color: ${global.colors.white};
//   min-height: 50px;
//   width: 100%;
//   flex: 1;
//   position: relative;
//   top: -15px;
//   padding: 10px;
// `;

// export const MinTitle = styled.View`
//   flex-direction: row;
//   justify-content: space-between;
// `;

// export const Title = styled.View`
//   flex-direction: row;
//   justify-content: space-between;
//   margin-top: 10px;
// `;

// export const About = styled.View`
//   flex-direction: row;
//   justify-content: space-between;
//   margin-top: 10px;
// `;

// export const Info = styled.View`
//   flex-direction: row;
//   margin-top: 10px;
// `;

// export const Buttons = styled.View`
//   flex-direction: row;
//   margin-top: 10px;
//   margin-bottom: 10px;
// `;
// export const ButtonWrapper = styled.View`
//   position: absolute;
//   bottom: 10px;
//   align-self: center;
//   justify-content: center;
//   align-items: center;
// `;
// export const FixedButton = styled.TouchableOpacity`
//   justify-content: center;
//   align-items: center;
//   width: 145px;
//   height: 30px;
//   background-color: ${global.colors.main};
//   border-radius: 10px;
// `;

// // Promotion-Screen

// export const PromotionWrapp = styled.View`
//   background-color: ${global.colors.white};
//   padding: 10px;
//   width: 100%;
// `;

// // Promotion-Food-Screen

// export const PromFoodWrap = styled.View`
//   border-top-right-radius: 20px;
//   border-top-left-radius: 20px;
//   box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
//   background-color: ${global.colors.white};
//   min-height: 50px;
//   width: 100%;
//   flex: 1;
//   position: relative;
//   top: -10px;
//   padding: 10px;
//   z-index: 10;
// `;

// // Food-Screen

// export const FoodScreenWrap = styled.View`
//   padding: 10px;
// `;

// export const ImgWrap = styled.View`
//   justify-content: center;
//   align-items: center;
// `;

// export const PriceBlock = styled.View`
//   flex-direction: row;
//   justify-content: space-between;
//   margin-top: 10px;
// `;

// export const CountBlock = styled.View`
//   flex-direction: row;
//   justify-content: space-between;
// `;

// export const FoodButtons = styled.View`
//   margin-top: 10px;
// `;

// export const FoodButtonWrapper = styled.View`
//   position: absolute;
//   bottom: 20px;
//   align-self: center;
//   justify-content: center;
//   align-items: center;
// `;

// export const ToCart = styled.TouchableOpacity`
//   justify-content: center;
//   align-items: center;
//   align-self: center;
//   width: ${global.strings.width - 30 + "px"};
//   height: 40px;
//   background-color: ${global.colors.main};
//   border-radius: 10px;
// `;

// // Profile-Screen

// export const ProfileWrap = styled.View`
//   margin-bottom: 1px;
// `;

// export const IconWrapp = styled.View`
//   align-items: center;
// `;

// export const InputWrapp = styled.View`
//   margin-top: 30px;
//   padding: 10px;
// `;

// export const TextWrapp = styled.View`
//   margin-top: 20px;
//   align-items: center;
// `;

// export const SubmitButton = styled.TouchableOpacity`
//   margin-top: 20px;
//   justify-content: center;
//   align-items: center;
//   background-color: ${global.colors.lightGreen};
//   width: 95%;
//   height: 40px;
//   border-radius: 10px;
//   margin-left: 10px;
// `;

// // Cart-Screen

// export const CartWrap = styled.View`
//   padding: 10px;
// `;

// export const CartTitle = styled.View`
//   flex-direction: row;
//   justify-content: space-between;
//   align-items: center;
// `;

// export const CartInputWrap = styled.View`
//   margin-top: 20px;
// `;

// export const DateWrap = styled.View`
//   box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.12);
// `;

// export const VisitorsBlock = styled.View`
//   margin-top: 20px;
// `;

// export const VisitorCount = styled.View`
//   padding: 10px;
//   margin-top: 5px;
//   flex-direction: row;
//   justify-content: space-between;
//   align-items: center;
//   background-color: ${global.colors.white};
//   border-radius: 20px;
//   box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.12);
//   width: 100%;
//   height: 40px;
// `;

// export const FoodMenuWrap = styled.View`
//   margin-top: 10px;
// `;

// export const PayButton = styled.TouchableOpacity`
//   margin-left: 10px;
//   width: 96%;
//   height: 40px;
//   background-color: ${global.colors.lightGreen};
//   border-radius: 10px;
//   justify-content: center;
//   align-items: center;
//   position: absolute;
//   bottom: 10px;
// `;

// // Search-Screen

// export const SearchWrap = styled.View`
//   padding: 10px;
// `;

// export const SearchInputWrap = styled.View`
//   flex-direction: row;
//   justify-content: space-between;
// `;

// export const SearchInput = styled.View`
//   padding: 10px;
//   flex-direction: row;
//   justify-content: space-between;
//   align-items: center;
//   width: 85%;
//   height: 40px;
//   background-color: ${global.colors.white};
//   border-radius: 10px;
//   box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.12);
// `;

// export const FilterWrap = styled.TouchableOpacity`
//   justify-content: center;
//   align-items: center;
//   width: 40px;
//   height: 40px;
//   background-color: ${global.colors.white};
//   border-radius: 10px;
//   box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.12);
// `;

// export const SearchButtonsWrap = styled.View`
//   margin-top: 10px;
//   flex-direction: row;
//   justify-content: space-between;
// `;

// export const SearchButton = styled.TouchableOpacity`
//   width: 47%;
//   height: 40px;
//   background-color: ${({ color = global.colors.blue }) => color};
//   box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.12);
//   justify-content: center;
//   align-items: center;
//   border-radius: 10px;
// `;

// export const MapButton = styled.TouchableOpacity`
//   margin-top: 10px;
//   flex: 1;
//   height: 40px;
//   flex-direction: row;
//   justify-content: center;
//   align-items: center;
//   background-color: ${global.colors.main};
//   box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.12);
//   border-radius: 10px;
// `;

// // Filter-Screen

// export const FilterWrapper = styled.View`
//   padding: 10px;
// `;

// export const FilterInputWrap = styled.View`
//   margin-top: 5px;
//   flex-direction: row;
//   justify-content: space-between;
//   width: 100%;
//   height: 40px;
//   background-color: ${global.colors.white};
//   border-radius: 10px;
//   box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.12);
// `;

// export const FilterButtonsWrap = styled.View`
//   flex-direction: row;
//   justify-content: space-between;
//   position: absolute;
//   bottom: 10px;
//   margin-left: 10px;
// `;

// export const FilterButton = styled.TouchableOpacity`
//   justify-content: center;
//   align-items: center;
//   height: 40px;
//   width: 49%;
//   background-color: ${({ color = global.colors.red }) => color};
//   border-radius: 10px;
//   box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.12);
// `;

// // Select-List-Screen

// export const ListWrap = styled.View`
//   padding: 10px;
// `;

// export const SelectButton = styled.TouchableOpacity`
//   justify-content: center;
//   align-items: center;
//   width: 95%;
//   height: 40px;
//   background-color: ${global.colors.main};
//   border-radius: 10px;
//   position: absolute;
//   bottom: 10px;
//   margin-left: 10px;
// `;
