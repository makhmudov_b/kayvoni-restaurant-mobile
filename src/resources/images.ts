const images = {
  food: require("../../assets/images/food.png"),
  phone: require("../../assets/images/phone.svg"),
  yaponaMama: require("../../assets/images/yaponaMama.png"),
  bgImage: require("../../assets/images/bg-image.png"),
  loading: require("../../assets/images/Kayvoni-loader1.gif"),
  // wok: require("../assets/img/wok.png"),
  // sushi: require("../assets/img/sushi.png"),
  // burger: require("../assets/img/burger.png"),
  // yaponaMamaRest: require("../assets/img/yaponaMamaRest.jpg"),
  // playground: require("../assets/img/playground.png"),
  // noSmoking: require("../assets/img/noSmoking.png"),
  // food: require("../assets/img/food.png"),
  // foodProm: require("../assets/img/foodProm.png"),
  // profileBg: require("../assets/img/profileBg.png"),
  // payMe: require("../assets/img/payMe.png"),
  // click: require("../assets/img/click.png"),
  // profilePhoto: require("../assets/img/profilePhoto.png"),
  // Star: require("../assets/img/bigStar.svg"),
  // EmptyStar: require("../assets/img/empty-star.svg"),
};

export default images;
