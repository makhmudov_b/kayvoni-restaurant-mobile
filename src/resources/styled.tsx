import React from "react";
import { View, Text } from "react-native";
import { AnyStyledComponent } from "styled-components";
import styled from "styled-components/native";
import global from "./global";
import { IButtonProp, IMarginProp, IPaddingProp, ITextProp } from "./types";

export const Margin = styled.View<IMarginProp>`
  margin: ${({ margin }) => (margin ? margin : 0)};
  margin-left: ${({ left }) => (left ? left : 0)};
  margin-right: ${({ right }) => (right ? right : 0)};
  margin-top: ${({ top }) => (top ? top : 0)};
  margin-bottom: ${({ bottom }) => (bottom ? bottom : 0)};
`;
export const Padding = styled.View<IPaddingProp>`
  padding: ${({ padding }) => (padding ? padding : 0)};
  padding-left: ${({ left }) => (left ? left : 0)};
  padding-right: ${({ right }) => (right ? right : 0)};
  padding-top: ${({ top }) => (top ? top : 0)};
  padding-bottom: ${({ bottom }) => (bottom ? bottom : 0)};
`;
export const HeaderWrap = styled.View`
  background-color: ${global.colors.main};
  padding: 0 15px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  height: 60px;
`;

export const TextFiled = styled.Text<ITextProp>`
  font-size: ${(props) => (props.size ? props.size : "16px")};
  color: ${({ color = global.colors.textColor }) => color};
  font-family: ${({ weight = "400" }) =>
    weight === "700"
      ? global.fonts.bold
      : weight === "400"
      ? global.fonts.book
      : global.fonts.semiBold};
`;

export const Clients = styled.TouchableOpacity`
  elevation: 5;
  width: 100%;
  height: 125px;
  margin-top: 10px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-top-left-radius: 20px;
  border-top-right-radius: 100px;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 100px;
  background-color: ${global.colors.white};
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
  overflow: hidden;
`;

export const ClientIcon = styled.View`
  position: relative;
  z-index: 1;
  width: 100px;
  height: 100px;
  background-color: white;
  border-radius: 50px;
  border-width: 1px;
  border-style: solid;
  border-color: #eee;
  align-items: center;
  justify-content: center;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
  text-align: center;
  elevation: 10;
`;

export const FoodMenu = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 10px 10px 10px 5px;
  background-color: ${global.colors.white};
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
  height: 60px;
  position: relative;
  margin-bottom: 10px;
  border-radius: 50px;
  elevation: 10;
`;

export const FoodImage = styled.View`
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
  padding: 4px;
  border-radius: ${49 / 2 + "px"};
  elevation: 6;
`;

export const ContentWrap = styled.View`
  elevation: 10;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background-color: ${global.colors.white};
  box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.12);
  height: 44px;
  padding: 10px;
  margin-bottom: 10px;
  overflow: hidden;
`;

export const RestWrap = styled.View`
  flex: 1;
  elevation: 10;
  min-height: 400px;
  background-color: ${global.colors.white};
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
  width: 100%;
  height: 44px;
  padding: 20px 10px 10px;
  margin-bottom: 10px;
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
  margin-top: 10px;
  overflow: hidden;
`;

export const ProfCircle = styled.View`
  justify-content: center;
  align-items: center;
  width: ${global.strings.width / 2.2}px;
  height: ${global.strings.width / 2.2}px;
  border-radius: 300px;
`;

export const ProfileInput = styled.TextInput`
  width: 100%;
  height: 30px;
  background-color: white;
  font-size: 14px;
  box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.12);
  border-radius: 10px;
  padding: 0px 15px;
  font-family: ${global.fonts.book};
`;

export const TextAreaClient = styled.TextInput`
  elevation: 5;
  width: 100%;
  height: 40px;
  background-color: white;
  font-size: 14px;
  box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.12);
  border-radius: 10px;
  padding: 0px 15px;
  margin-bottom: 10px;
`;

export const Button = styled.TouchableOpacity<IButtonProp>`
  flex: 1;
  height: 40px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: ${({ color = global.colors.main }) => color};
  padding: 0px 15px;
  border-radius: 10px;
  margin-top: auto;
  overflow: hidden;
`;
export const RegisterBlock = styled.ImageBackground`
  width: 100%;
  height: 100%;
  justify-content: center;
  padding: 30px;

  /* align-items: center; */
`;

export const InputBlock = styled.View`
  margin-bottom: 20px;
`;

export const RowBlock = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 10px;
`;

export const OrderItem = styled.TouchableOpacity<any>`
  elevation: 5;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 10px;
  background-color: ${({ color = global.colors.white }: any) => color};
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
  min-height: 40px;
  margin-bottom: 10px;
  border-top-right-radius: 20px;
  border-top-left-radius: 20px;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;
`;

export const DatePickerWrap = styled.View`
  flex: 1;
  height: 30px;
  background-color: white;
  box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.12);
  border-radius: 10px;
  padding: 0px 15px;
`;
