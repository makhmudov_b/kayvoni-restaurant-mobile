const colors = {
  main: "#440000",
  pink: "#FF5858",
  red: "#EB5757",
  white: "#FFFFFF",
  blue: "#00A3FF",
  brown: "#450000",
  green: "#3E623E",
  lightGreen: "#6FCF97",
  darkBlue: "#2F80ED",
  yellow: "#EFA335",
  lightYellow: "#FFD600",
  textColor: "#333333",
  textColor2: "#BABABA",
};

export default colors;
