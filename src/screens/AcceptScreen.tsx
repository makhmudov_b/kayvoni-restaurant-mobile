import { RouteProp, useRoute } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { View, Text } from "react-native";

import { useDispatch, useSelector } from "react-redux";

import OrderInfo from "../components/OrderInfo";
import { getOrderById, acceptOrder } from "../redux/actions/orderAction";
import global from "../resources/global";

import { TextFiled, ProfileInput, Button } from "../resources/styled";
import LoadingScreen from "./LoadingScreen";

type ParamList = {
  id?: any;
};

export default function AcceptScreen(props) {
  const dispatch = useDispatch();

  const route = useRoute<RouteProp<ParamList, "id">>();
  const { id } = route.params;

  const [value, setValue] = useState("");

  const onTextChanged = (text) => {
    // code to remove non-numeric characters from text
    setValue(text);
  };

  const onRecieve = () => {
    if (!value.trim().length) {
      alert("Заполните все!");
      return;
    }
    acceptOrder(id, value, setValue(""));
    // ;
  };

  const { order } = useSelector((state: any) => state.order);
  const { loader } = useSelector((state: any) => state.loading);

  useEffect(() => {
    dispatch(getOrderById(id));
  }, []);

  if (loader || !order) return <LoadingScreen />;
  return (
    <View style={{ padding: 10, flex: 1 }}>
      <>
        <OrderInfo user={order.user} time={order.reserve_time} />
        <TextFiled
          size="14px"
          weight="700"
          color={global.colors.main}
          style={{ marginBottom: 5 }}
        >
          Укажите номер столика
        </TextFiled>
        <ProfileInput
          keyboardType="numeric"
          style={{ height: 40, marginBottom: 10 }}
          value={value}
          onChangeText={(text) => onTextChanged(text)}
        />
        <View style={{ marginTop: "auto", marginBottom: 20, height: 30 }}>
          <Button onPress={onRecieve} color={global.colors.lightGreen}>
            <TextFiled color="white" weight="700">
              Отправить
            </TextFiled>
          </Button>
        </View>
      </>
    </View>
  );
}
