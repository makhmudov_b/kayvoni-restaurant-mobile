import React from "react";
import { View, Text, Image } from "react-native";
import global from "../resources/global";

export default function LoadingScreen({ type = "main" }) {
  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: type == "main" ? "rgba(68,0,0,.8)" : "white",
      }}
    >
      <View
        style={{
          width: 220,
          height: 220,
          borderRadius: 110,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "#FFE7C3",
        }}
      >
        <Image source={global.images.loading} />
      </View>
    </View>
  );
}
