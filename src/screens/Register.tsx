import React, { useState } from "react";
import {
  View,
  Text,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  StatusBar,
} from "react-native";
import { Dimensions } from "react-native";

import global from "../resources/global";
import {
  RegisterBlock,
  InputBlock,
  ProfileInput,
  TextFiled,
  Button,
} from "../resources/styled";
import Kayvoni from "../../assets/images/kayvoni.svg";
// import { useCustonInput } from "../utils/inputHook";
import { useDispatch, useSelector } from "react-redux";
import { auth } from "../redux/actions/authActions";
import { ActivityIndicator } from "react-native-paper";

export default function Register({ navigation }) {
  const dispatch = useDispatch();

  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");

  const { loading } = useSelector((state: any) => state.auth);
  const handleLogin = () => {
    if (phone.length === 9 && password.length > 3) {
      dispatch(auth({ phone, password }, () => navigation.replace("Home")));
    }
  };
  return (
    <RegisterBlock source={global.images.bgImage}>
      <StatusBar barStyle="light-content" />
      <Kayvoni
        width={180}
        height={180}
        style={{ alignSelf: "center", marginBottom: 70, marginTop: 70 }}
      />
      <View>
        <InputBlock>
          <TextFiled size="13px" color="white" style={{ marginBottom: 5 }}>
            Логин
          </TextFiled>
          <ProfileInput
            value={phone}
            keyboardType="phone-pad"
            placeholder={"991234567"}
            maxLength={9}
            onChangeText={(text) => setPhone(text)}
          />
        </InputBlock>
        <InputBlock>
          <TextFiled size="13px" color="white" style={{ marginBottom: 5 }}>
            Пароль
          </TextFiled>
          <ProfileInput
            secureTextEntry={true}
            value={password}
            maxLength={15}
            placeholder={"*******"}
            onChangeText={(text) => setPassword(text)}
          />
        </InputBlock>
      </View>
      <View style={{ alignItems: "center", marginBottom: 22 }}>
        <TextFiled size="13px" color="white" style={{ marginBottom: 5 }}>
          Войдя в аккаунт вы соглашаетесь с
        </TextFiled>
        <TextFiled size="13px" color={global.colors.yellow}>
          условиями использования приложения
        </TextFiled>
      </View>
      <View style={{ marginTop: "auto", height: 30 }}>
        <Button color={global.colors.lightGreen} onPress={handleLogin}>
          <TextFiled color="white" weight="700">
            Войти
          </TextFiled>
        </Button>
      </View>
      {loading && (
        <View
          style={{
            position: "absolute",
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            zIndex: 1,
            backgroundColor: "rgba(255,255,255,.5)",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <ActivityIndicator size="large" />
        </View>
      )}
    </RegisterBlock>
  );
}
