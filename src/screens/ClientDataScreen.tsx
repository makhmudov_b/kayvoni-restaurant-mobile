import { RouteProp, useNavigation, useRoute } from "@react-navigation/native";
import React, { useEffect } from "react";
import { View, Text, Dimensions } from "react-native";
import { ScrollView } from "react-native-gesture-handler";

import global from "../resources/global";
import {
  RowBlock,
  TextFiled,
  OrderItem,
  TextAreaClient,
  Button,
} from "../resources/styled";
import OrderInfo from "../components/OrderInfo";
import { useDispatch, useSelector } from "react-redux";
import { getOrderById } from "../redux/actions/orderAction";
import LoadingScreen from "./LoadingScreen";
import { getStatusColor, getStatusName } from "../utils/helpers";

export default function ClientDataScreen({ route, navigation }) {
  const dispatch = useDispatch();

  const { id } = route.params;

  const { order, innerLoading, innerError } = useSelector(
    (state: any) => state.order
  );

  // useEffect(() => {
  //   dispatch(getOrderById(id));
  // }, []);
  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      dispatch(getOrderById(id));
    });
    return unsubscribe;
  }, [navigation]);

  if (innerLoading || !order) return <LoadingScreen />;
  if (innerError) return <Text>{innerError.message}</Text>;

  return (
    <View style={{ backgroundColor: "#fff", flex: 1 }}>
      <ScrollView>
        <View
          style={{
            padding: 10,
          }}
        >
          {order.user && (
            <OrderInfo user={order.user} time={order.reserve_time} />
          )}
          <View>
            <OrderItem color={getStatusColor(order.status)}>
              <TextFiled color="white" size="12px" weight="700">
                Статус
              </TextFiled>
              <TextFiled color="white" size="12px" weight="700">
                {getStatusName(order.status)}
              </TextFiled>
            </OrderItem>
            {!!order.table_number && (
              <OrderItem>
                <TextFiled size="12px" weight="700">
                  Номер столика
                </TextFiled>
                <TextFiled size="12px" weight="700">
                  {order.table_number}
                </TextFiled>
              </OrderItem>
            )}
            <OrderItem color={global.colors.main}>
              <TextFiled color="white" size="12px" weight="700">
                Кол-во поситителей
              </TextFiled>
              <TextFiled color="white" size="12px" weight="700">
                {order.fee_per_seat} x {order.seats_count}
                {" = "}
                {order.seats_price} сум
              </TextFiled>
            </OrderItem>
            {!!order.comment && (
              <>
                <TextFiled size="14px" weight="700" style={{ marginBottom: 5 }}>
                  Предпочтение
                </TextFiled>
                <TextAreaClient editable={false}>
                  {order.comment}
                </TextAreaClient>
              </>
            )}
            {order.order_item.map((item, key) => (
              <OrderItem key={key}>
                <TextFiled size="12px" weight={"700"}>
                  х{item.count}
                </TextFiled>
                <View style={{ flex: 1, paddingHorizontal: 8 }}>
                  <TextFiled size="12px">
                    {item.food.name_ru} {item.params.name_ru}
                  </TextFiled>
                </View>
                <TextFiled size="12px" weight={"700"}>
                  {item.total_price} сум
                </TextFiled>
              </OrderItem>
            ))}
            <RowBlock
              style={{
                paddingBottom: 10,
                borderBottomWidth: 1,
                borderBottomColor: global.colors.textColor2,
              }}
            >
              <TextFiled size="14px" weight="700">
                Сумма заказа:
              </TextFiled>
              <TextFiled size="14px">{order.real_price} сум</TextFiled>
            </RowBlock>
          </View>
          <RowBlock>
            <TextFiled size="14px" weight="700">
              Для Kayvoni {order.system_percentage}%
            </TextFiled>
            <TextFiled size="14px">{order.system_price} сум</TextFiled>
          </RowBlock>
          <RowBlock>
            <TextFiled size="14px" weight="700">
              Для Ресторана {order.restaurant_percentage}%
            </TextFiled>
            <TextFiled size="14px">{order.restaurant_price} сум</TextFiled>
          </RowBlock>
          <RowBlock style={{ marginBottom: 23 }}>
            <TextFiled size="14px" weight="700">
              Оплачено:
            </TextFiled>
            <TextFiled size="14px">{order.payed_amount} сум</TextFiled>
          </RowBlock>
        </View>
      </ScrollView>
      {order.status == 1 && (
        <RowBlock
          style={{
            position: "absolute",
            bottom: 20,
            alignSelf: "center",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "row",
            paddingHorizontal: 10,
          }}
        >
          <Button
            color={global.colors.red}
            onPress={() => {
              navigation.replace("Response", {
                recieve: false,
                id: order.id,
              });
            }}
          >
            <TextFiled size="13px" color="white" weight="700">
              Отклонить
            </TextFiled>
          </Button>
          <View style={{ width: 10 }} />
          <Button
            color={global.colors.lightGreen}
            onPress={() => {
              navigation.replace("Response", {
                recieve: true,
                id: order.id,
              });
            }}
          >
            <TextFiled size="13px" color="white" weight="700">
              Принять
            </TextFiled>
          </Button>
        </RowBlock>
      )}
    </View>
  );
}
