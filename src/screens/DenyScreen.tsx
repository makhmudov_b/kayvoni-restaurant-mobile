import { RouteProp, useRoute } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { View, Text } from "react-native";

import DropDownPicker from "react-native-dropdown-picker";
import Icon from "react-native-vector-icons/Feather";
import { useDispatch, useSelector } from "react-redux";

import OrderInfo from "../components/OrderInfo";
import { getOrderById, denyOrder } from "../redux/actions/orderAction";
import global from "../resources/global";

import { TextFiled, Button } from "../resources/styled";

type ParamList = {
  id?: any;
};

export default function DenyScreen(props) {
  const dispatch = useDispatch();

  const route = useRoute<RouteProp<ParamList, "id">>();
  const { id } = route.params;

  const [response, setResponse] = useState("");
  const onDeny = () => {
    if (!response.trim().length) {
      alert("Заполните все!");
      return;
    }
    denyOrder(id, response);
  };

  const { order } = useSelector((state: any) => state.order);
  const { loader } = useSelector((state: any) => state.loading);

  useEffect(() => {
    dispatch(getOrderById(id));
  }, []);

  if (loader || !order) return <Text>Loading...</Text>;
  return (
    <View style={{ padding: 10, flex: 1 }}>
      <>
        <OrderInfo />
        <TextFiled
          size="14px"
          weight="700"
          color={global.colors.main}
          style={{ marginBottom: 5 }}
        >
          Укажите причину отказа
        </TextFiled>
        <DropDownPicker
          items={[
            {
              label: "Holiday",
              value: "Holiday",
              hidden: true,
            },
            {
              label: "Sanitariya",
              value: "Sanitariya",
            },
            {
              label: "Full",
              value: "Full",
            },
          ]}
          defaultValue={response}
          containerStyle={{ height: 40 }}
          style={{ backgroundColor: "#fafafa" }}
          itemStyle={{
            justifyContent: "flex-start",
          }}
          dropDownStyle={{ backgroundColor: "#fafafa" }}
          onChangeItem={(item) => setResponse(item.value)}
          placeholder="Выберите из списка"
        />
        <View style={{ marginTop: "auto", marginBottom: 20, height: 30 }}>
          <Button color={global.colors.red} onPress={() => onDeny()}>
            <TextFiled color="white" weight="700">
              Отправить
            </TextFiled>
          </Button>
        </View>
      </>
    </View>
  );
}
