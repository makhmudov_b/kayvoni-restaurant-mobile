import React from "react";
import { View, Text } from "react-native";
import Success from "../../assets/images/success-logo.svg";
import { TextFiled } from "../resources/styled";

export default function SuccessScreen() {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Success />
      <TextFiled size="18px" weight="600">
        проведено успешно
      </TextFiled>
    </View>
  );
}
