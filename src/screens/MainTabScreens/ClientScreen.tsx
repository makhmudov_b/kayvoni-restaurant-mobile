import { useNavigation } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { View, Text, FlatList, TouchableOpacity } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { getOrderList } from "../../redux/actions/orderAction";
import global from "../../resources/global";
import RefreshIcon from "../../../assets/images/refresh.svg";

import {
  Clients,
  TextFiled,
  ClientIcon,
  RowBlock,
  Button,
} from "../../resources/styled";
import { getStatusColor } from "../../utils/helpers";
import LoadingScreen from "../LoadingScreen";
import { getUserInfo } from "../../redux/actions/userAction";
import { ScrollView } from "react-native-gesture-handler";

const helpers = [
  { color: global.colors.red, text: "отклонен" },
  { color: global.colors.lightYellow, text: "в ожидании" },
  { color: global.colors.lightGreen, text: "принят" },
  { color: global.colors.darkBlue, text: "закончен" },
];
export default function ClientScreen({ navigation }) {
  const dispatch = useDispatch();

  const { orders, ordersLoading, ordersError } = useSelector(
    (state: any) => state.order
  );
  const [filtered, setFiltered] = useState();
  const getOrder = () => {
    dispatch(getUserInfo());
    return dispatch(getOrderList());
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      getOrder();
    });
    return unsubscribe;
  }, [navigation]);
  if (ordersLoading) return <LoadingScreen />;
  if (ordersError) return <Text>{ordersError.message}</Text>;
  const rejected = orders.filter((item) => item.status == -2);
  const payed = orders.filter((item) => item.status == 1);
  const accepted = orders.filter((item) => item.status == 2);
  const done = orders.filter((item) => item.status == 3);
  const OrderBlock = ({ item }) => {
    return (
      <Clients
        onPress={() => {
          navigation.navigate("ClientData", {
            id: item.id,
          });
        }}
      >
        <View
          style={{
            width: "70%",
            height: "100%",
            backgroundColor: "white",
            padding: 10,
          }}
        >
          <RowBlock
            style={{
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <TextFiled
              size="14px"
              weight="700"
              style={{ marginBottom: 5, maxWidth: 145 }}
            >
              {item.user.name}
            </TextFiled>
            <View
              style={{
                width: 15,
                height: 15,
                backgroundColor: `${getStatusColor(item.status)}`,
                borderRadius: 7.5,
                marginLeft: 10,
                alignSelf: "flex-end",
                marginBottom: 5,
              }}
            ></View>
          </RowBlock>
          <TextFiled
            size="12px"
            color={global.colors.textColor2}
            weight="400"
            style={{ marginBottom: 5 }}
          >
            {item.user.phone}
          </TextFiled>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              marginBottom: 5,
            }}
          >
            <TextFiled size="12px" weight={700}>
              Цена предоплаты:
            </TextFiled>
            <TextFiled size="12px">
              {item.payed_amount}
              сум
            </TextFiled>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <TextFiled size="12px" weight={700}>
              Бронь на дату:
            </TextFiled>
            <TextFiled size="12px">{item.reserve_time.slice(0, -3)}</TextFiled>
          </View>
        </View>
        <ClientIcon>
          <TextFiled size="24px">№{item.id}</TextFiled>
        </ClientIcon>
      </Clients>
    );
  };
  return (
    <View style={{ paddingBottom: 55 }}>
      <View
        style={{
          flexDirection: "row",
          width: "100%",
          alignItems: "center",
          paddingHorizontal: 10,
          paddingTop: 5,
        }}
      >
        <View style={{ flex: 1, flexDirection: "row", flexWrap: "wrap" }}>
          {helpers.map((el) => (
            <View
              style={{
                minWidth: global.strings.width / 3.2,
                flexDirection: "row",
                alignItems: "center",
              }}
              key={el.color}
            >
              <View
                style={{
                  width: 15,
                  height: 15,
                  backgroundColor: el.color,
                  borderRadius: 7.5,
                  marginRight: 5,
                }}
              />
              <TextFiled>{el.text}</TextFiled>
            </View>
          ))}
        </View>

        <View>
          <TouchableOpacity
            style={{
              backgroundColor: global.colors.main,
              padding: 6,
              borderRadius: 8,
            }}
            onPress={() => getOrder()}
          >
            <RefreshIcon color="#fff" />
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        style={{
          flexDirection: "row",
          padding: 10,
        }}
      >
        <Button
          onPress={() => setFiltered(orders)}
          color={global.colors.white}
          style={{ marginRight: 5 }}
        >
          <TextFiled weight={700}>Все</TextFiled>
        </Button>
        <Button
          color={global.colors.red}
          style={{ marginLeft: 5 }}
          onPress={() => setFiltered(rejected)}
        >
          <TextFiled weight={700} color={global.colors.white}>
            Отклонен
          </TextFiled>
        </Button>
        <Button
          color={global.colors.lightYellow}
          style={{ marginLeft: 5 }}
          onPress={() => setFiltered(payed)}
        >
          <TextFiled weight={700} color={global.colors.white}>
            В ожидании
          </TextFiled>
        </Button>
        <Button
          color={global.colors.lightGreen}
          style={{ marginLeft: 5 }}
          onPress={() => setFiltered(accepted)}
        >
          <TextFiled weight={700} color={global.colors.white}>
            Принят
          </TextFiled>
        </Button>
        <Button
          color={global.colors.blue}
          style={{ marginLeft: 5 }}
          onPress={() => setFiltered(done)}
        >
          <TextFiled weight={700} color={global.colors.white}>
            Закончен
          </TextFiled>
        </Button>
      </ScrollView>
      <FlatList
        data={filtered || orders}
        keyExtractor={(item, key) => key.toString() + item.status + item.id}
        renderItem={({ item }) => <OrderBlock item={item} />}
        contentContainerStyle={{ padding: 10, paddingBottom: 70 }}
      />
    </View>
  );
}
