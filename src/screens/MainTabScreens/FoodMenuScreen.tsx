import React, { useEffect, useState } from "react";
import { View, Text, Image, ActivityIndicator } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
// import { cloneDeep } from "lodash";

import global from "../../resources/global";
import { useDispatch, useSelector } from "react-redux";
import { getFoodList, swichFoodById } from "../../redux/actions/foodAction";
import LoadingScreen from "../LoadingScreen";
import FoodItem from "../../components/FoodItem";

// const _ = require("lodash");

export default function FoodMenuScreen() {
  const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const dispatch = useDispatch();
  const { foods, loading, error, switchLoading } = useSelector(
    (state: any) => state.food
  );

  useEffect(() => {
    dispatch(getFoodList());
  }, []);

  const enableHandle = (id, callback) => {
    dispatch(swichFoodById(id, callback));
  };

  if (loading) return <LoadingScreen />;
  if (error) return <Text>{error.message}</Text>;
  const string = JSON.stringify(foods);
  const copy = JSON.parse(string);
  return (
    <>
      <ScrollView
        style={{
          flex: 1,
          position: "relative",
          height: global.strings.height,
        }}
      >
        <View style={{ padding: 10 }}>
          {!!copy.length &&
            copy.map((food) => (
              <FoodItem key={food.id} food={food} enableHandle={enableHandle} />
            ))}
        </View>
      </ScrollView>
      {switchLoading && (
        <View
          style={{
            position: "absolute",
            // height: global.strings.height,
            top: 0,
            bottom: 0,
            right: 0,
            left: 0,
            backgroundColor: "rgba(212, 209, 209, 0.8)",
            alignItems: "center",
            justifyContent: "center",
            zIndex: 10,
          }}
        >
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      )}
    </>
  );
}
