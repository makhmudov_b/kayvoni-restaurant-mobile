import React, { useEffect, useState } from "react";
import { View, Text, Image, Platform, Linking } from "react-native";
import Phone from "../../../assets/images/phone.svg";
import global from "../../resources/global";
import Indev from "../../../assets/images/indev.svg";
import IndevText from "../../../assets/images/indevText.svg";
import {
  ContentWrap,
  TextFiled,
  RestWrap,
  ProfCircle,
  Button,
} from "../../resources/styled";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../redux/actions/authActions";
import LoadingScreen from "../LoadingScreen";
import { getStatics } from "../../redux/actions/profileAction";

const phone = "+998 953405005"

export default function ProfileScreen({ navigation }) {
  const { restaurant, profit, ordersCount, loading, error } = useSelector(
    (state: any) => state.profile
  );
  const dispatch = useDispatch();
  
  const callNumber = (phone) => {
    const link = `tel:${phone}`;
    Linking.canOpenURL(link).then((supported) => {
      if (!supported) {
        alert("Phone number is not available");
      } else {
        return Linking.openURL(link);
      }
    });
  };

  const userLogOut = () => {
    dispatch(logout(() => navigation.replace("Register")));
  };
  const loadStats = () => {
    dispatch(getStatics());
  };
  useEffect(() => {
    loadStats();
  }, []);
  if (loading || !restaurant) return <LoadingScreen />;
  if (error) return <Text>{error.message}</Text>;
  return (
    <View
      style={{
        backgroundColor: "#fff",
        flex: 1,
      }}
    >
      <ContentWrap>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <TextFiled size="14px" font={global.fonts.semiBold}>
            Поддержка:
          </TextFiled>
          <TextFiled size="12px" style={{ marginLeft: 10, marginTop: 2 }}>
            {phone}
          </TextFiled>
        </View>
        <Phone onPress={() => callNumber(phone)} />
      </ContentWrap>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          padding: 10,
          marginBottom: 10,
          height: 90,
        }}
      >
        <Image
          style={{ width: 90, height: 90, borderRadius: 45 }}
          source={{ uri: restaurant.logo }}
        />
        <TextFiled
          size="18px"
          weight="700"
          style={{ maxWidth: 190, marginLeft: 20 }}
        >
          {restaurant.name}
        </TextFiled>
      </View>
      <RestWrap>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            marginBottom: 10,
          }}
        >
          <ProfCircle style={{ backgroundColor: global.colors.darkBlue }}>
            <TextFiled color="white" weight={"bold"}>
              Профит в UZS
            </TextFiled>
            <TextFiled color="white" weight={"bold"}>
              {profit}
            </TextFiled>
          </ProfCircle>
          <ProfCircle
            style={{
              backgroundColor: global.colors.lightGreen,
              marginLeft: 10,
            }}
          >
            <TextFiled color="white" weight={"bold"}>
              Кол-во заказов
            </TextFiled>
            <TextFiled color="white" weight={"bold"}>
              {ordersCount}
            </TextFiled>
          </ProfCircle>
        </View>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            marginBottom: 10,
            marginTop: "auto",
          }}
        >
          <IndevText />
          <Indev style={{ marginLeft: 6 }} />
        </View>
        <View style={{ height: 30, marginBottom: 10 }}>
          <Button
            color={global.colors.red}
            style={{ borderRadius: 10 }}
            onPress={() => userLogOut()}
          >
            <TextFiled size="13px" color="white" weight="700">
              Выйти
            </TextFiled>
          </Button>
        </View>
      </RestWrap>
    </View>
  );
}
