import React, { Component, createRef } from "react";
import { Alert, StyleSheet, Text, View } from "react-native";
import { Agenda, LocaleConfig } from "react-native-calendars";
import { connect } from "react-redux";
import {
  getOrderList,
  getOrderListWithDate,
} from "../../redux/actions/orderAction";
import global from "../../resources/global";
import { ClientIcon, Clients, TextFiled } from "../../resources/styled";
import LoadingScreen from "../LoadingScreen";

// const testIDs = require("../testIDs");
const helpers = [
  { color: global.colors.lightGreen, text: "-до 10 заказов" },
  { color: global.colors.lightYellow, text: "-до 50 заказов" },
  { color: global.colors.red, text: "-больше 50 заказов" },
];

interface ILoginScreenProps {
  navigation: any;
  loading?: any;
  error?: any;
  loadData?: any;
  calendar?: any;
  notification?: any;
}
LocaleConfig.locales["ru"] = {
  monthNames: [
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь",
  ],
  monthNamesShort: [
    "Янв.",
    "Фев.",
    "Мар.",
    "Апр.",
    "Май",
    "Июн",
    "Июл.",
    "Авг.",
    "Сен.",
    "Окт.",
    "Ноя.",
    "Дек.",
  ],
  dayNames: [
    "Понедельник",
    "Вторник",
    "Среда",
    "Четверг",
    "Пятница",
    "Суббота",
    "Воскресенье",
  ],
  dayNamesShort: ["Пн.", "Вт.", "Ср.", "Чт.", "Пт.", "Сб.", "Вс."],
  today: "Сегодня",
};
LocaleConfig.defaultLocale = "ru";
class CalendarScreen extends Component<ILoginScreenProps> {
  unsubscribe: any;
  renderItem(item) {
    return (
      <View style={{ paddingRight: 10, paddingBottom: 10, paddingTop: 10 }}>
        <TextFiled weight="700" size="14px">
          {item.reserve_time.slice(0, -3)}
        </TextFiled>
        <Clients
          style={{ marginTop: 5 }}
          onPress={() =>
            this.props.navigation.navigate("ClientData", { id: item.id })
          }
        >
          <View
            style={{
              width: "70%",
              height: "100%",
              backgroundColor: "white",
              padding: 10,
              justifyContent: "space-between",
              marginTop: 10,
            }}
          >
            <TextFiled
              size="14px"
              weight="700"
              style={{ marginBottom: 10, maxWidth: 145 }}
            >
              {item.user.name}
            </TextFiled>
            <TextFiled
              size="12px"
              color={global.colors.textColor2}
              weight="400"
              style={{ marginBottom: 5 }}
            >
              +998{item.user.phone}
            </TextFiled>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 5,
              }}
            >
              <TextFiled size="12px" weight={700}>
                На {item.seats_count} персон
              </TextFiled>
              <TextFiled size="12px" weight={700}>
                Столик №{item.table_number}
              </TextFiled>
            </View>
          </View>
          <ClientIcon>
            <TextFiled size="24px">№{item.id}</TextFiled>
          </ClientIcon>
        </Clients>
      </View>
    );
  }

  renderEmptyDate() {
    return (
      <View style={styles.emptyDate}>
        <Text>This is empty date!</Text>
      </View>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split("T")[0];
  }
  focusListener: any;
  getData(): void {
    const date = new Date();
    const lastMonth = new Date(date.getFullYear(), date.getMonth() - 1, 1);
    const nextMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    this.props.loadData(lastMonth, nextMonth);
  }
  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener("focus", () => {
      this.getData();
    });
  }
  componentWillUnmount() {
    this.unsubscribe();
  }

  componentDidUpdate(prevProps) {
    if (
      JSON.stringify(prevProps.notification) !==
      JSON.stringify(this.props.notification)
    ) {
      this.getData();
    }
  }

  render() {
    if (this.props.error) return <Text>{this.props.error.message}</Text>;
    if (this.props.loading) return <LoadingScreen />;
    return (
      <>
        {/* <View
          style={{
            flexDirection: "row",
            width: "100%",
            flexWrap: "wrap",
            padding: 10,
            minHeight: 60,
            backgroundColor: "#fff",
          }}
        >
          {helpers.map((el) => (
            <View
              style={{
                marginRight: 40,
                flexDirection: "row",
                alignItems: "center",
              }}
              key={el.color}
            >
              <View
                style={{
                  width: 15,
                  height: 15,
                  backgroundColor: el.color,
                  borderRadius: 7.5,
                  marginRight: 5,
                }}
              ></View>
              <TextFiled>{el.text}</TextFiled>
            </View>
          ))}
        </View> */}
        <Agenda
          testID={"agenda"}
          items={this.props.calendar}
          // loadItemsForMonth={(month) => this.loadItems(month)}
          renderItem={this.renderItem.bind(this)}
          renderEmptyDate={() => this.renderEmptyDate()}
          rowHasChanged={(r1, r2) => this.rowHasChanged(r1, r2)}
          scrollEnabled={true}
          theme={{
            dotColor: global.colors.main,
            selectedDayBackgroundColor: global.colors.main,
          }}
          renderDay={() => <View style={{ width: 10 }} />}
          hideExtraDays={true}
        />
      </>
    );
  }
}
function mapStateToProps(state) {
  return {
    loading: state.order.loading,
    error: state.order.error,
    calendar: state.order.calendar,
    notification: state.user.notification,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    loadData: (from: any, to: any) => dispatch(getOrderListWithDate(from, to)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(CalendarScreen);

const styles = StyleSheet.create({
  item: {
    backgroundColor: "white",
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30,
  },
});
