import {
  RouteProp,
  TabActions,
  useNavigation,
  useRoute,
} from "@react-navigation/native";
import { NavigationScreenProp, NavigationRoute } from "react-navigation";
import React, { useEffect, useState } from "react";
import { View, Text } from "react-native";

import DropDownPicker from "react-native-dropdown-picker";
import Icon from "react-native-vector-icons/Feather";
import { useDispatch, useSelector } from "react-redux";

import OrderInfo from "../components/OrderInfo";
import {
  getOrderById,
  acceptOrder,
  denyOrder,
} from "../redux/actions/orderAction";
import global from "../resources/global";

import { TextFiled, ProfileInput, Button } from "../resources/styled";
import LoadingScreen from "./LoadingScreen";
import { getReasons } from "../redux/actions/profileAction";

export default function ResponseScreen({ route, navigation }) {
  const dispatch = useDispatch();
  const { id, recieve } = route.params;
  const { order, innerLoading, error } = useSelector(
    (state: any) => state.order
  );
  const { reason, loading, error: reasonErr } = useSelector(
    (state: any) => state.reason
  );

  const [value, setValue] = useState("");
  const [response, setResponse] = useState("");

  // const onTextChanged = (text) => {
  //   setValue(text);
  // };
  const navigateSuccess = () => {
    const navigate = TabActions.jumpTo("Client");
    navigation.dispatch(navigate);
    navigation.replace("Home");
  };
  const onRecieve = () => {
    if (!value.trim().length) {
      alert("Заполните все!");
      return;
    }
    dispatch(acceptOrder(id, value, () => navigateSuccess()));
    // ;
  };
  const onDeny = () => {
    if (!response.trim().length) {
      alert("Заполните все!");
      return;
    }
    dispatch(denyOrder(id, response, () => navigateSuccess()));
  };

  useEffect(() => {
    dispatch(getOrderById(id));
    if (!recieve) {
      dispatch(getReasons());
    }
  }, []);

  if (innerLoading || !order) return <LoadingScreen />;
  if (error || reasonErr)
    return (
      <Text>
        {error && error.message}
        {reasonErr && reasonErr.message}
      </Text>
    );
  return (
    <View style={{ padding: 10, flex: 1 }}>
      {!!recieve ? (
        <>
          <OrderInfo user={order.user} time={order.reserve_time} />
          <TextFiled
            size="14px"
            weight="700"
            color={global.colors.main}
            style={{ marginBottom: 5 }}
          >
            Укажите номер столика
          </TextFiled>
          <ProfileInput
            keyboardType="numeric"
            returnKeyType={"done"}
            style={{ height: 40, marginBottom: 10 }}
            value={value}
            onChangeText={(text) => setValue(text)}
          />
          <View style={{ marginTop: "auto", marginBottom: 20, height: 30 }}>
            <Button onPress={onRecieve} color={global.colors.lightGreen}>
              <TextFiled color="white" weight="700">
                Отправить
              </TextFiled>
            </Button>
          </View>
        </>
      ) : (
        <>
          <OrderInfo user={order.user} time={order.reserve_time} />
          <TextFiled
            size="14px"
            weight="700"
            color={global.colors.main}
            style={{ marginBottom: 5 }}
          >
            Укажите причину отказа
          </TextFiled>
          {!loading && !!reason && (
            <DropDownPicker
              items={reason.map((el) => {
                return { value: el.name_ru, label: el.name_ru };
              })}
              defaultValue={response}
              containerStyle={{ height: 40 }}
              style={{ backgroundColor: "#fafafa" }}
              itemStyle={{
                justifyContent: "flex-start",
              }}
              dropDownStyle={{ backgroundColor: "#fafafa" }}
              onChangeItem={(item) => setResponse(item.value)}
              placeholder="Выберите из списка"
            />
          )}
          <View style={{ marginTop: "auto", marginBottom: 20, height: 30 }}>
            <Button color={global.colors.red} onPress={() => onDeny()}>
              <TextFiled color="white" weight="700">
                Отправить
              </TextFiled>
            </Button>
          </View>
        </>
      )}
    </View>
  );
}
