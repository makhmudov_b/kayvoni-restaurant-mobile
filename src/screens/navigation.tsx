import React, { useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
//
import global from "../resources/global";
//
import CalendarScreen from "./MainTabScreens/CalendarScreen";
import ClientScreen from "./MainTabScreens/ClientScreen";
import FoodMenuScreen from "./MainTabScreens/FoodMenuScreen";
import ProfileScreen from "./MainTabScreens/ProfileScreen";
//
import CalendarIcon from "../../assets/images/calendar.svg";
import FoodMenuIcon from "../../assets/images/document.svg";
import ProfileIcon from "../../assets/images/profile.svg";
import ClientIcon from "../../assets/images/wallet.svg";
import { View } from "react-native";
import Header from "../components/Header";
import Register from "./Register";
import ClientDataScreen from "./ClientDataScreen";
import ResponseScreen from "./ResponseScreen";
import SuccessScreen from "./SuccessScreen";
import { TextFiled } from "../resources/styled";
import { useSelector } from "react-redux";

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

const BottomTabScreens = () => {
  const { orders_count = 0, loading = false } = useSelector(
    (state: any) => state.user
  );
  return (
    <Tab.Navigator
      barStyle={{
        height: 70,
        backgroundColor: global.colors.main,
        borderTopColor: global.colors.white,
        borderTopWidth: 1,
      }}
      shifting={true}
      labeled={false}
      activeColor={global.colors.main}
      inactiveColor={global.colors.white}
      backBehavior={`none`}
    >
      <Tab.Screen
        name={"Client"}
        component={ClientScreen}
        options={{
          tabBarIcon: ({ color, focused }) => (
            <View
              style={{
                backgroundColor: focused
                  ? global.colors.white
                  : global.colors.main,
                borderRadius: 50,
                padding: 4,
              }}
            >
              <ClientIcon width={24} height={24} color={color} />
              <View
                style={{
                  position: "absolute",
                  right: -5,
                  top: -5,
                  width: 13,
                  height: 13,
                  backgroundColor: "red",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 13 / 2,
                }}
              >
                {!loading && (
                  <TextFiled size="8px" weight="700" color="white">
                    {orders_count}
                  </TextFiled>
                )}
              </View>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name={"Calendar"}
        component={CalendarScreen}
        options={{
          tabBarIcon: ({ color, focused }) => (
            <View
              style={{
                backgroundColor: focused
                  ? global.colors.white
                  : global.colors.main,
                borderRadius: 50,
                padding: 4,
              }}
            >
              <CalendarIcon width={24} height={24} color={color} />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name={"FoodMenu"}
        component={FoodMenuScreen}
        options={{
          tabBarIcon: ({ color, focused }) => (
            <View
              style={{
                backgroundColor: focused
                  ? global.colors.white
                  : global.colors.main,
                borderRadius: 50,
                padding: 4,
              }}
            >
              <FoodMenuIcon width={24} height={24} color={color} />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name={"Profile"}
        component={ProfileScreen}
        options={{
          tabBarIcon: ({ color, focused }) => (
            <View
              style={{
                backgroundColor: focused
                  ? global.colors.white
                  : global.colors.main,
                borderRadius: 50,
                padding: 4,
              }}
            >
              <ProfileIcon width={24} height={24} color={color} />
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default function AppContainer() {
  const { token } = useSelector((state: any) => state.auth);
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={!!token ? "Home" : "Register"}>
        <Stack.Screen
          name="Register"
          component={Register}
          options={{ header: () => null }}
        />
        <Stack.Screen
          name="Home"
          options={{ header: () => <Header /> }}
          component={BottomTabScreens}
        />
        <Stack.Screen
          name="ClientData"
          component={ClientDataScreen}
          options={{ header: () => <Header enableBack={true} /> }}
        />
        <Stack.Screen
          name="Response"
          component={ResponseScreen}
          options={{ header: () => <Header enableBack={true} /> }}
        />
        <Stack.Screen
          name="Success"
          component={SuccessScreen}
          options={{ header: () => <Header enableBack={true} /> }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
