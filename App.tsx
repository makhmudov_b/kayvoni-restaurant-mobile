import React, { useEffect, useRef, useState } from "react";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import * as Font from "expo-font";
import AppContainer from "./src/screens/navigation";
import { store, persistor } from "./src/redux/storeConfig";
import LoadingScreen from "./src/screens/LoadingScreen";
import {
  ActivityIndicator,
  Platform,
  Text,
  TextInput,
  View,
} from "react-native";
import * as Notifications from "expo-notifications";
import * as Permissions from "expo-permissions";
import Constants from "expo-constants";
import {
  getUserInfo,
  updateNotification,
  updateUserInfo,
} from "./src/redux/actions/userAction";
import global from "./src/resources/global";

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

export default function App() {
  const [loading, setLoading] = useState<boolean>(true);
  const dispatch: any = store.dispatch;
  const state = store.getState();
  const { token } = state.auth;
  const { user } = state.user;
  const notificationListener = useRef<any>();
  const getUserData = () => {
    if (token) {
      dispatch(getUserInfo());
      if (user) {
        registerForPushNotificationsAsync(user.notification_token);
      }
    }
  };
  const registerForPushNotificationsAsync = async (settedToken: any) => {
    const {
      status: existingStatus,
    } = await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus !== "granted") {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    if (finalStatus !== "granted") {
      return;
    }
    const notificationToken = await Notifications.getDevicePushTokenAsync();
    if (notificationToken != settedToken) {
      const body = {
        notification_token: notificationToken.data,
      };
      dispatch(updateUserInfo(body));
    }
    if (Platform.OS === "android") {
      Notifications.setNotificationChannelAsync("default", {
        name: "kayvoni",
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: global.colors.main,
      });
    }
  };
  useEffect(() => {
    Font.loadAsync({
      Bold: require("./assets/fonts/bold.ttf"),
      Book: require("./assets/fonts/book.ttf"),
      SemiBold: require("./assets/fonts/semibold.ttf"),
    }).then(() => {
      getUserData();
      setLoading(false);
      notificationListener.current = Notifications.addNotificationReceivedListener(
        (notification) => {
          if (notification) dispatch(updateNotification(notification));
        }
      );
      return () => {
        Notifications.removeNotificationSubscription(
          notificationListener.current
        );
      };
    });
  }, []);
  if (loading)
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "rgba(212, 209, 209, 0.8)",
          alignItems: "center",
          justifyContent: "center",
          zIndex: 10,
        }}
      >
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <AppContainer />
      </PersistGate>
    </Provider>
  );
}
